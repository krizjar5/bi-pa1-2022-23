#include <stdio.h>

void all_nums_rec(int len, int current) {
    if (len == 0) {
        printf("%d\n", current);
        return;
    }

    all_nums_rec(len - 1, current * 2);
    all_nums_rec(len - 1, current * 2 + 1);
}

void all_nums(int len) { all_nums_rec(len, 0); }

void all_nums_no_seq_rec(int len, int current) {
    if ((current & 7) == 7) {
        return;
    }

    if (len == 0) {
        printf("%d\n", current);
        return;
    }

    all_nums_no_seq_rec(len - 1, current * 2);
    all_nums_no_seq_rec(len - 1, current * 2 + 1);
}

void all_nums_no_seq(int len) { all_nums_no_seq_rec(len, 0); }

int main(void) {
    all_nums(20);
    all_nums_no_seq(4);
}
