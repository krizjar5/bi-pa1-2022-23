#include <assert.h>

int rec_max(int *arr, size_t length) {
    // Zakladni reseni
    if (length == 1) {
        return *arr;
    }

    // Reseni podproblemu
    int remainder_max = rec_max(arr + 1, length - 1);

    // Zpracovani vysledku
    if (remainder_max < *arr) {
        return *arr;
    } else {
        return remainder_max;
    }
}

int main(void) {
    int test0[] = {8};
    int test1[] = {1, 4, 9, 3, 2, 6};
    int test2[] = {4, 4, 4, 4, 4, 4, 4};

    assert(rec_max(test0, 6) == 8);
    assert(rec_max(test1, 6) == 9);
    assert(rec_max(test2, 6) == 4);
}
