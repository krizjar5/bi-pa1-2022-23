#include <stdio.h>

long factorial(long n) {
    if (n == 0) {
        return 1;
    }

    return n * factorial(n - 1);
}

int main(void) {
    for (int i = 1; i <= 10; i++) {
        printf("Faktorial %d je %ld\n", i, factorial(i));
    }
    return 0;
}
