#include <stdio.h>
#include <stdlib.h>

typedef struct TNode {
    int value;
    struct TNode *next;
} TNode;

TNode *listCreate() { return NULL; }

TNode *listPushFront(TNode *front, int value) {
    TNode *new_front = (TNode *)malloc(sizeof(TNode));
    new_front->value = value;
    new_front->next = front;
    return new_front;
}

void listPrint(TNode *front) {
    while (front != NULL) {
        printf("%d ", front->value);
        front = front->next;
    }
    printf("\n");
}

void listDelete(TNode *front) {
    while (front != NULL) {
        TNode *next = front->next;
        free(front);
        front = next;
    }
}

TNode *listMerge(TNode *front1, TNode *front2) {
    TNode *front;
    if (front1->value < front2->value) {
        front = front1;
        front1 = front1->next;
    } else {
        front = front2;
        front2 = front2->next;
    }

    TNode *back = front;

    while (front1 != NULL && front2 != NULL) {
        if (front1->value < front2->value) {
            back->next = front1;
            back = front1;
            front1 = front1->next;
        } else {
            back->next = front2;
            back = front2;
            front2 = front2->next;
        }
    }
    TNode *remaining = front1 == NULL ? front2 : front1;
    back->next = remaining;
    return front;
}

int main(void) {
    TNode *list1 = listCreate();
    for (int i = 10; i > 0; i -= 2) {
        list1 = listPushFront(list1, i);
    }
    listPrint(list1);

    TNode *list2 = listCreate();
    for (int i = 20; i > 0; i -= 3) {
        list2 = listPushFront(list2, i);
    }
    listPrint(list2);

    TNode *merged = listMerge(list1, list2);
    listPrint(merged);

    listDelete(merged);
}
