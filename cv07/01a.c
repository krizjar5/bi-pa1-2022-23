#include <stdio.h>
#include <limits.h>

void fill_zeroes(int pole[], size_t count) {
    for (size_t i = 0; i < count; i++) {
        pole[i] = 0;
    }
}

void print_array(int arr[], size_t count) {
    for (size_t i = 0; i < count; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(void) {
    int arr[25];

    fill_zeroes(arr, sizeof(arr) / sizeof(*arr));
    print_array(arr, sizeof(arr) / sizeof(*arr));
}