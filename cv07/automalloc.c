#include <stdio.h>
#include <stdlib.h>

int main(void) {
    char *loaded_string1 = NULL;
    char *loaded_string2 = NULL;
    size_t len1 = 0;
    size_t len2 = 0;
    getline(&loaded_string1, &len1, stdin);
    getline(&loaded_string2, &len2, stdin);
    printf("%s%s", loaded_string2, loaded_string1);
    free(loaded_string1);
    free(loaded_string2);
}
