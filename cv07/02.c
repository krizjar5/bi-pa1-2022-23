#include <stdio.h>
#include <string.h>

void print_from_start(char string[]) {
    for (size_t i = 0; string[i] != '\0'; i++) {
        putchar(string[i]); // printf("%c", string[i]);
    }
    putchar('\n'); // printf("\n")
}

void print_reverse(char string[]) {
    size_t length = strlen(string);
    for (size_t i = 0; i < length; i++) {
        putchar(string[length - i - 1]);
    }
    putchar('\n');
}

int main(void) {
    char string[] = "stepech je nejlepsi!";
    print_from_start(string);
    print_reverse(string);
}