/**
 * Napište program, který od uživatele načítá celá čísla dokud nezadá 0 a vypisuje součet.
 */

#include <stdio.h>

int main (void) {
	int res, x, sum = 0;

	printf("Zadejte cisla [zadavani ukoncite 0]:\n");
	while ((res = scanf("%d", &x)) == 1 && x != 0) {
		sum += x;
		printf("Prubezny soucet %d.\n", sum);
	}

	if (res != 1) {
		printf("Nespravny vstup.\n");
		return 1;
	}

	printf("Finalni soucet je %d.\n", sum);
	return 0;
}

