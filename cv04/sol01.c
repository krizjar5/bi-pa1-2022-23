/**
 * Napište program, který od uživatele načítá `n` celých čísel a vypisuje maximum.
 */

#include <stdio.h>
#include <limits.h>

int main (void) {
	int n, x, max = INT_MIN;

	printf("Zadejte pocet cisel:\n");
	if (scanf("%d", &n) != 1 || n <= 0) {
		printf("Nespravny vstup.\n");
		return 1;
	}

	for (int i = 0; i < n; ++i) {
		printf("Zadejte %d. cislo:\n", i + 1);
		if (scanf("%d", &x) != 1) {
			printf("Nespravny vstup.\n");
			return 2;
		}

		if (x > max)
			max = x;
	}

	printf("Maximum je %d.\n", max);
	return 0;
}

