/**
 * Napište program, který od uživatele načítá celá čísla do ukončení vstupu (`EOF`).
   Program vypisuje průměr z těchto čísel, při nesprávném vstupu se ukončí.
 */

#include <stdio.h>

int main(void) {
	int sum = 0, count = 0, x, res;
	printf("Zadejte cisla:\n");
	
	/* Uložíme si návratovou hodnotu funkce `scanf` a následně ji použijeme pro vyhodnocení podmínky.
	   Pokud se konverze podaří, cyklus se vykoná.
	*/
	while ((res = scanf("%d", &x)) == 1) {
		sum += x;
		count++;

		/* Nezapomeneme přetypovat, abychom se vyhnuli celočíselnému dělení. */
		printf("avg = %g\n",(double)sum / count);
	}
	
	if (res != EOF) {
		printf("Nespravny vstup.\n");
		return 1;
	}

	/* Program ukončíme na Linuxu klávesovou zkratkou CTRL+D */
	return 0;
}

