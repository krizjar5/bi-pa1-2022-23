#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int *buf;
    size_t len;
    size_t cap;
} TArray;

TArray *make_array() {
    TArray *arr = (TArray *)malloc(sizeof(TArray));
    arr->len = 0;
    arr->cap = 5;
    arr->buf = (int *)malloc(arr->cap * sizeof(int));
    return arr;
}

int get_elem(TArray *arr, size_t i) {
    assert(i < arr->len);
    return arr->buf[i];
}

void push_back(TArray *arr, int i) {
    if (arr->len == arr->cap) {
        arr->cap *= 2;
        arr->buf = (int *)realloc(arr->buf, arr->cap * sizeof(int));
    }
    arr->buf[arr->len] = i;
    arr->len++;
}

void free_array(TArray *arr) {
    free(arr->buf);
    free(arr);
}

void print_array(TArray *arr) {
    for (size_t i = 0; i < arr->len; i++) {
        printf("%d ", get_elem(arr, i));
    }
    printf("\n");
}

int main(void) {
    printf("Zadej cisla:\n");
    int input;
    TArray *arr = make_array();
    while (scanf("%d", &input) == 1) {
        push_back(arr, input);
    }
    printf("Tvoje cisla jsou: \n");
    print_array(arr);
    free_array(arr);
}
