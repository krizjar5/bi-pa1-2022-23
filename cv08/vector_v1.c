#include <stdio.h>
#include <stdlib.h>

typedef int TYPE;

void printArray(TYPE * data, size_t count) {
	for(size_t i = 0; i < count; i++) {
		printf("%d ", data[i]);
	}
	printf("\n");
}

void push_back(TYPE ** data, size_t * count, size_t * max, int x) {
	if (*count >= *max){
		*max *= 2;
		*data = (TYPE*)realloc(*data, *max * sizeof(TYPE));
	}
	(*data)[*count] = x;
	(*count)++;
}

int main() {
	size_t count = 0, max = 1;
	int x, res;
	TYPE * data = (TYPE*)malloc(sizeof(TYPE) * max);
		
	while ((res = scanf("%d", &x)) == 1) {
		push_back(&data, &count, &max, x);
	}
	
	if (res != EOF) {
		printf("Nespravny vstup.\n");
		free(data);
		return 1;
	}
	
	printArray(data, count);
	free(data);
	return 0;
}

