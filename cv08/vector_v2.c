#include <stdio.h>
#include <stdlib.h>

typedef int TYPE;

typedef struct TArray {
	size_t count;
	size_t max;
	TYPE * data;
} TARRAY;

void printArray(TARRAY * array) {
	for(size_t i = 0; i < array->count; i++) {
		printf("%d ", array->data[i]);
	}
	printf("\n");
}

void push_back(TARRAY * array, int x) {
	if (array->count >= array->max){
		array->max *= 2;
		array->data = (TYPE*)realloc(array->data, array->max * sizeof(TYPE));
	}
	array->data[array->count] = x;
	array->count++;
}

TARRAY createArray() {
	TARRAY array;
	array.count = 0;
	array.max = 1;
	array.data = (TYPE*)malloc(sizeof(TYPE) * array.max);
	return array;
}

int main() {
	int x, res;
	TARRAY array = createArray();
		
	while ((res = scanf("%d", &x)) == 1) {
		push_back(&array, x);
	}
	
	if (res != EOF) {
		printf("Nespravny vstup.\n");
		free(array.data);
		return 1;
	}
	
	printArray(&array);
	free(array.data);
	return 0;
}

