#include <stdio.h>
#include <stdlib.h>

void printArray(int * arr, size_t len) {
	for (size_t i = 0; i < len; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main() {
	size_t len;
	if (scanf("%lu", &len) != 1) {
		return 1;
	}
	
	int * arr = (int*)malloc(sizeof(int) * len);
	
	for (size_t i = 0; i < len; i++) {
		arr[i] = i;
	}
	
	printArray(arr, len);
	
	free(arr);
	return 0;
}

