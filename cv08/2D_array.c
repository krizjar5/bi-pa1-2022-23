#include <stdio.h>
#include <stdlib.h>

void printArray(int ** arr, size_t rows, size_t columns) {
	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < columns; j++) {
			printf("%2d ", arr[i][j]);
		}
		printf("\n");
	}
}

int main() {
	size_t rows, columns;
	if (scanf ("%lu %lu", &rows, &columns) != 2) {
		return 1;
	}
	
	int ** arr = (int**)malloc(sizeof(int*) * rows);
	for (size_t i = 0; i < rows; i++) {
		arr[i] = (int*)malloc(sizeof(int) * columns);
	}
	
	for (size_t i = 0; i < rows; i++) {
		for (size_t j = 0; j < columns; j++) {
			arr[i][j] = i * rows + j;
		}
	}
	
	printArray(arr, rows, columns);
	
	
	for (size_t i = 0; i < rows; i++) {
		free(arr[i]);
	}
	free(arr);
	return 0;
}

