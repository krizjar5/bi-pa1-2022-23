#include <assert.h>

int isLeap (int year) {
    if (year % 400 == 0)
        return 1;
    if (year % 100 == 0)
        return 0;
    return year % 4 == 0;
}

int daysInMonth (int month, int year) {
    switch (month) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            return 31;
        case 4:
        case 6:
        case 9:
        case 11:
            return 30;
        case 2:
            return 28 + isLeap(year);
        default:
            return 0;
    }
}

int validDate (int day, int month, int year) {
    if (year < 1600 || month < 1 || month > 12 || day < 1 || day > daysInMonth(month, year))
        return 0;
    return 1;
}

int main (void) {
    assert(isLeap(2000) == 1);
    assert(isLeap(2001) == 0);
    assert(isLeap(2004) == 1);
    assert(isLeap(2100) == 0);
    assert(isLeap(2200) == 0);
    assert(isLeap(2400) == 1);


    assert(daysInMonth( 2, 2000) == 29);
    assert(daysInMonth( 2, 2001) == 28);
    assert(daysInMonth(12, 2000) == 31);
    assert(daysInMonth(11, 2000) == 30);


    assert(validDate(-1,  1,  2000) == 0);
    assert(validDate(32,  1,  2000) == 0);
    assert(validDate( 1, 13,  2000) == 0);
    assert(validDate( 1, -1,  2000) == 0);
    assert(validDate( 1,  1,  1599) == 0);
    assert(validDate( 1,  1, -2000) == 0);

    assert(validDate( 1,  1, 2000) == 1);
    assert(validDate( 1,  2, 2000) == 1);
    assert(validDate(29,  2, 2000) == 1);
    assert(validDate(29,  2, 2001) == 0);
    assert(validDate( 1, 12, 2000) == 1);
    assert(validDate(31, 12, 2000) == 1);
    assert(validDate( 6,  7, 3600) == 1);
    assert(validDate(29,  2, 3600) == 1);

    return 0;
}
