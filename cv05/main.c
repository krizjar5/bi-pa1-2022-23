#include <stdio.h>
#include <float.h> // DBL_EPSILON
#include <math.h>  // fabs()

/*
              x == y
          x - y == 0
  -eps <= x - y <= eps
	  | x - y | <= eps

	pořád se jedná o absolutní porovnání
	-> potřebujeme zavést relativní porovnání, tj. škálovat podle dat, se kterými počítáme

	  | x - y | <= DBL_EPSILON * škálovací faktor * řádová_chyba
	  | x - y | <= DBL_EPSILON * 1000 * (|x| + |y|)
	                
*/

int isEqual (double x, double y) {
	return fabs(x - y) <= DBL_EPSILON * 1000 * (fabs(x) + fabs(y));
}

int main() {
	
	printf("x == y ? %d\n", 0.1 + 0.2 == 0.3);
	
	printf("x == y ? %d\n", isEqual(0.1 + 0.2, 0.3));

	return 0;
}

